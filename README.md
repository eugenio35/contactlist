![Alt Text](https://i.imgflip.com/2htm3i.gif)

# Contact List

Android App that displays a list of users from the [Randon User API](https://randomuser.me/). 

## Getting Started

Feel free to clone the project and import it to Android Studio. If you want a quick look please install the APK file included [**Here.**](https://bitbucket.org/eugenio35/contactlist/downloads/contact_list.apk)

### Cloning the project

Execute the git clone command.

```
git clone https://eugenio35@bitbucket.org/eugenio35/contactlist.git
```

## Built With

* [Gson](https://github.com/google/gson) - A Java serialization/deserialization library
* [OkHttp3](http://square.github.io/okhttp/) - An HTTP & HTTP/2 client for Android and Java applications
* [Glide](https://github.com/bumptech/glide) - An image loading and caching library for Android
* [CircleImageView](https://github.com/hdodenhof/CircleImageView) - A circular ImageView for Android