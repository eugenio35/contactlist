package net.sweatworks.contactlist.model.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.contact_entry.view.*
import kotlinx.android.synthetic.main.favorite_contact_entry.view.*
import net.sweatworks.contactlist.R
import net.sweatworks.contactlist.model.Contact

/**
 * Contacts Adapter that handles both Recycler Views [Favorotes] and [Contacts]
 *
 *
 * @see Contact Responsible of handling the List of Contact and inflate the Entry View
 *
 *
 * @property contactList
 * @property context
 * @property mListener
 * @property isFavorites
 */
class ContactsAdapter(contactList: ArrayList<Contact>, isFavorites:Boolean, mListener:ItemListener) : RecyclerView.Adapter<ContactsAdapter.ViewHolder>() {

    var contactList: ArrayList<Contact>? = null
    var context: Context? = null
    var mListener: ItemListener? = null
    var isFavorites:Boolean? = null

    init {
        this.contactList = contactList
        this.mListener = mListener
        this.isFavorites = isFavorites
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactsAdapter.ViewHolder {
        val layoutEntry = if(isFavorites!!) R.layout.favorite_contact_entry else R.layout.contact_entry
        val v: View = LayoutInflater.from(parent.context)
                .inflate(layoutEntry, parent, false)
        context = parent.context!!

        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return contactList!!.size
    }

    override fun onBindViewHolder(holder: ContactsAdapter.ViewHolder, position: Int) {
        val currentContact = contactList?.get(position)

        holder.contact = currentContact
        val userName = currentContact?.name
        holder.contactName?.text = userName?.first.plus(" ").plus(userName?.last)

        Glide.with(context!!).load(currentContact?.picture?.thumbnail).into(holder.contactImage)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener{

        val contactImage = if(isFavorites!!) itemView.img_favorite_contact else itemView.img_contact
        val contactName = if(isFavorites!!) itemView.favorite_contact_name else itemView.contact_name
        var contact: Contact? = null

        init {
            //On click listener to the hole view.
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            if (mListener != null) {
                //Post the selected Contact
                mListener?.onItemClick(contact!!, isFavorites!!)
            }
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    interface ItemListener {
        fun onItemClick(contact: Contact, isFavorites: Boolean)
    }
}