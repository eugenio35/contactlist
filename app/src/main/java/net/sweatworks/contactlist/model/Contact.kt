package net.sweatworks.contactlist.model

import java.io.Serializable

/**
 * Contact Object
 *
 *
 * Model class of the application
 *
 * @property name
 * @property email
 * @property phone
 * @property cell
 * @property location
 * @property picture
 * @property isFavorite
 *
 */
class Contact: Serializable{

    lateinit var name: Name
    lateinit var email: String
    lateinit var phone: String
    lateinit var cell:String
    lateinit var location:Location
    lateinit var picture:Picture
    var isFavorite = false

    constructor() {}

    constructor(name: Name, email: String, phone: String, cell: String, location: Location, picture: Picture) {
        this.name = name
        this.email = email
        this.phone = phone
        this.cell = cell
        this.location = location
        this.picture = picture
    }

    override fun toString(): String {
        return name.first.plus(" ").plus(name.last)
    }

    /**
     *
     *
     * Name class
     *
     * @property title
     * @property first
     * @property last
     *
     */
    inner class Name : Serializable{
        lateinit var title: String
        lateinit var first: String
        lateinit var last: String

        constructor()

        constructor(title: String, first: String, last: String) {
            this.title = title
            this.first = first
            this.last = last
        }
    }

    /**
     *
     *
     * Location class
     *
     * @property street
     * @property city
     * @property state
     *
     */
    inner class Location : Serializable{
        lateinit var street: String
        lateinit var city: String
        lateinit var state: String

        constructor()

        constructor(street: String, city: String, state: String) {
            this.street = street
            this.city = city
            this.state = state
        }
    }

    /**
     *
     *
     * Pictture class
     *
     * @property thumbnail
     * @property medium
     * @property large
     *
     */
    inner class Picture : Serializable{
        lateinit var thumbnail: String
        lateinit var medium: String
        lateinit var large: String

        constructor()

        constructor(thumbnail: String, medium: String, large: String) {
            this.thumbnail = thumbnail
            this.medium = medium
            this.large = large
        }
    }
}

