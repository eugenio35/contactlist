package net.sweatworks.contactlist.controller

import android.os.AsyncTask
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import net.sweatworks.contactlist.model.Contact
import net.sweatworks.contactlist.util.APIConstants
import okhttp3.*
import okhttp3.HttpUrl

/**
 * Contacts Controller AsyncTask
 *
 * AsyncTask that fetch and parse the list of Users from the https://randomuser.me/ Api
 *
 * @see AsyncTask
 * @see OkHttpClient
 *
 * @property mListener
 *
 */
class ContactsController : AsyncTask<Any, Void, ArrayList<Contact>>() {

    private var mListener: ContactsCallbackListener? = null

    /**
     * Makes a Get request with a list of query params to filter the List of Users
     *
     * @param pageRequest
     * @param callBackListener
     *
     */
    override fun doInBackground(vararg params: Any): ArrayList<Contact>? {
        val pageRequest = params[0] as String
        val callBackListener = params[1] as ContactsCallbackListener
        mListener = callBackListener

        var contactList: ArrayList<Contact> = arrayListOf()

        val httpBuilder = HttpUrl.parse(APIConstants.BASE_DOMAIN)!!.newBuilder()

        // Setting every query param to the URL
        httpBuilder.addQueryParameter(APIConstants.PAGE_PARAMETER, pageRequest)
        httpBuilder.addQueryParameter(APIConstants.RESULTS_PARAMETER, "50")
        httpBuilder.addQueryParameter(APIConstants.SEED_PARAMETER, APIConstants.CUSTOM_SEED)
        httpBuilder.addQueryParameter(APIConstants.INC_PARAMETER, APIConstants.INC_VALUES)

        val client = OkHttpClient()
        val request = Request.Builder()
                .url(httpBuilder.build())
                .build()

        val response = client.newCall(request).execute()

        val body = response?.body()?.string()
        val gson = GsonBuilder().create()

        val jsonObject = gson.fromJson(body, JsonObject::class.java)
        val results = jsonObject.get("results").asJsonArray

        for(contact in results.iterator()){
            val newContact = gson.fromJson(contact, Contact::class.java)
            contactList.add(newContact)
        }

        return contactList
    }

    /**
     * Post the updated List to Contact to the Home Activity
     *
     * @param result
     *
     */
    override fun onPostExecute(result: ArrayList<Contact>) {
        mListener!!.onFetchComplete(result)
    }

    /**
     * Callback Interface for the subscribers
     *
     * @param result
     *
     */
    interface ContactsCallbackListener {
        fun onFetchComplete(newContactList: ArrayList<Contact>)
    }
}

