package net.sweatworks.contactlist.util

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import net.sweatworks.contactlist.model.Contact

/**
 * Shared Preferences Manager
 *
 * This class has is the Main entry point to store and retrieve contacts
 *
 * @see Contact This class has is the Main entry point to store and retrieve contacts
 *
 */
class SharedPreferencesManager{

    private var preferences: SharedPreferences? = null
    private val gson = Gson()
    private val type = object : TypeToken<List<Contact>>() {}.type

    constructor(context: Context){
        this.preferences = context.getSharedPreferences("net.sweatworks", Context.MODE_PRIVATE)
    }

    /**
     * Adds a [contact] to the favorites list in Shared Preferences
     *
     * @param contact the new contact to be added
     *
     */
    fun saveFavorites(contact:Contact){
        var favJson = preferences!!.getString("favorites_json", "")
        var favoriteContactList = arrayListOf<Contact>()

        if(favJson.isNotEmpty()){
            favoriteContactList = gson.fromJson(favJson!!, type)
        }

        favoriteContactList.add(contact)
        favJson = gson.toJson(favoriteContactList)

        preferences!!.edit().putString("favorites_json", favJson).apply()
    }

    /**
     * Gets the favorites list from Shared Preferences
     *
     * @return the favorite contact list
     *
     */
    fun getFavorites():ArrayList<Contact>{
        val favJson = preferences!!.getString("favorites_json", "")
        var favoriteContactList = arrayListOf<Contact>()

        if(favJson.isNotEmpty()){
            favoriteContactList = gson.fromJson(favJson!!, type)
        }

        return favoriteContactList
    }
}