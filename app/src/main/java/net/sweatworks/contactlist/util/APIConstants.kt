package net.sweatworks.contactlist.util

/**
 * A group of *Constants*.
 *
 * This class has the static fields used to consume the https://randomuser.me/ Api
 *
 */

object APIConstants {
    var BASE_DOMAIN = "https://randomuser.me/api/"
    var PAGE_PARAMETER = "page"
    var RESULTS_PARAMETER = "results"
    var SEED_PARAMETER = "seed"
    var CUSTOM_SEED = "custom_seed"
    var INC_PARAMETER = "inc"
    var INC_VALUES = "name,email,phone,cell,location,picture"
}