package net.sweatworks.contactlist.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_user_details.*

import net.sweatworks.contactlist.R
import net.sweatworks.contactlist.model.Contact
import android.content.DialogInterface
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import net.sweatworks.contactlist.util.SharedPreferencesManager
import android.provider.ContactsContract
import android.content.Intent

/**
 * User Details Activity
 *
 * Responsible for displaying every detail of the Contact.
 *
 * @property selectedContact
 * @property prefManager
 *
 */
class UserDetailsActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var selectedContact:Contact
    private lateinit var prefManager:SharedPreferencesManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_details)
        getSelectedContact()
        setupViews()
        fillContactInfo(selectedContact)
    }

    private fun getSelectedContact() {
        selectedContact = intent.getSerializableExtra("selected_contact") as Contact
    }

    private fun setupViews(){
        prefManager = SharedPreferencesManager(context = this)
        btn_add_contact.setOnClickListener(this)
        if(supportActionBar != null){
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
        }
    }

    private fun fillContactInfo(contact:Contact){
        val name = contact.name
        val location = contact.location
        Glide.with(this).load(contact.picture.large).into(contact_img)
        txt_name.text = name.first.plus(" ").plus(name.last)
        txt_email.text = contact.email
        txt_phone.text = contact.phone
        txt_mobile.text = contact.cell
        txt_address.text = location.street.plus(" ").plus(location.city).plus(location.state)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.contact_details_menu,menu)
        if(selectedContact.isFavorite!!){
            menu?.findItem(R.id.action_no_fav)?.isVisible = false
        }else{
            menu?.findItem(R.id.action_fav)?.isVisible = false
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId == android.R.id.home){
            this.finish()
        }else if(item?.itemId == R.id.action_no_fav){
            showAddFavoriteConfirmation()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun showAddFavoriteConfirmation(){
        val builder = AlertDialog.Builder(this)
        builder.setMessage(getString(R.string.add_favorites_confirmation))
        builder.setCancelable(true)

        builder.setPositiveButton(
                getString(R.string.btn_accept),
                DialogInterface.OnClickListener { dialog, _ ->
                    saveToFavorites()
                    invalidateOptionsMenu()

                    Snackbar.make(main_coordinator, getString(R.string.saved_successfully), Snackbar.LENGTH_LONG).show()

                    dialog.cancel()
                })

        builder.setNegativeButton(
                getString(R.string.btn_cancel),
                DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })

        val alertDialog = builder.create()
        alertDialog.show()
    }

    private fun saveToFavorites(){
        selectedContact.isFavorite = true
        prefManager.saveFavorites(selectedContact)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            btn_add_contact.id->{
                addToPhoneContacts()
            }
        }
    }

    private fun addToPhoneContacts() {
        val name = selectedContact.name
        val intent = Intent(Intent.ACTION_INSERT)
        intent.type = ContactsContract.Contacts.CONTENT_TYPE
        intent.putExtra(ContactsContract.Intents.Insert.NAME, name.first.plus(" ").plus(name.last))
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, selectedContact.cell)
        startActivity(intent)
    }
}
