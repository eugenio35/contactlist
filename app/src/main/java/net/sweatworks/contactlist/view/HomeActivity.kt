package net.sweatworks.contactlist.view

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AbsListView
import kotlinx.android.synthetic.main.activity_home.*
import net.sweatworks.contactlist.R
import net.sweatworks.contactlist.controller.ContactsController
import net.sweatworks.contactlist.model.Contact
import net.sweatworks.contactlist.model.adapter.ContactsAdapter
import net.sweatworks.contactlist.util.SharedPreferencesManager
import android.widget.AdapterView
import android.widget.ArrayAdapter

/**
 * Home Activity
 *
 * Launcher Activity, shows both Favorite and Contact Recycler View. Listens for OnItemClicks in
 * the Recycler View and opens the Detail View.
 *
 * @property currentPage
 * @property isScrolling
 * @property currentPage
 * @property totalItems
 * @property scrollOutOfScreen
 * @property gridManager
 * @property contactList
 * @property prefManager
 * @property searchView
 * @property searchAutoComplete
 * @property searchMenuItem
 *
 */
class HomeActivity : AppCompatActivity(), ContactsController.ContactsCallbackListener, ContactsAdapter.ItemListener, AdapterView.OnItemClickListener {

    private var currentPage = 1
    private var isScrolling = false
    private var currentItems: Int? = null
    private var totalItems: Int? = null
    private var scrollOutOfScreen: Int? = null
    private var gridManager: GridLayoutManager? = null
    private lateinit var contactList: ArrayList<Contact>
    private lateinit var prefManager: SharedPreferencesManager
    private lateinit var searchView: SearchView
    private lateinit var searchAutoComplete: SearchView.SearchAutoComplete
    private lateinit var searchMenuItem: MenuItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setupViews()
        getUsers()
    }

    private fun setupViews() {
        prefManager = SharedPreferencesManager(context = this)
        contactList = arrayListOf()
        setupContactsRecyclerView()
        setupFavoritesContactsRecyclerView()
    }

    private fun setupContactsRecyclerView() {
        //Setting a 3 Columns Recycler View in a vertical position
        gridManager = GridLayoutManager(applicationContext, 3, GridLayoutManager.VERTICAL, false)
        contacts_recycler_view.layoutManager = gridManager
        contacts_recycler_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true
                }
            }

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                currentItems = gridManager?.childCount
                totalItems = gridManager?.itemCount
                scrollOutOfScreen = gridManager?.findFirstVisibleItemPosition()

                //If the items shown in the screen and the items scrolled out equals the
                //total items we need to fetch more items.
                if (isScrolling && (currentItems?.plus(scrollOutOfScreen!!) == totalItems)) {
                    isScrolling = false
                    currentPage = currentPage.plus(1)
                    getUsers()
                }
            }
        })
    }

    private fun setupFavoritesContactsRecyclerView() {
        val favoritesContacts = prefManager.getFavorites()
        if (favoritesContacts.isNotEmpty()) {
            favorites_contacts_container.visibility = View.VISIBLE
            val favAdapter = ContactsAdapter(contactList = favoritesContacts, isFavorites = true, mListener = this)
            favorites_contacts_recycler_view.layoutManager = GridLayoutManager(applicationContext, 1, GridLayoutManager.HORIZONTAL, false)
            favorites_contacts_recycler_view.adapter = favAdapter
        }

    }

    private fun getUsers() {
        progress_bar.visibility = View.VISIBLE
        ContactsController().execute(currentPage.toString(), this)
    }

    override fun onFetchComplete(newContactList: ArrayList<Contact>) {
        //Append the newly fetched List to the previous and refresh the adapter
        this.contactList.addAll(newContactList)
        val adapter = ContactsAdapter(contactList = this.contactList, isFavorites = false, mListener = this)
        //Get the previous state of the Recycler view in order to maintain focus
        val parcelableData = contacts_recycler_view.layoutManager.onSaveInstanceState()
        contacts_recycler_view.adapter = adapter
        contacts_recycler_view.layoutManager.onRestoreInstanceState(parcelableData)

        //Refresh the user selection in the Search Bar autocomplete
        invalidateOptionsMenu()
        progress_bar.visibility = View.GONE
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home_menu, menu)

        configureSearchView(menu)

        return super.onCreateOptionsMenu(menu)
    }

    private fun configureSearchView(menu: Menu?) {
        // Retrieve the SearchView and plug it into SearchManager
        searchMenuItem = menu?.findItem(R.id.action_search)!!

        searchView = searchMenuItem?.actionView as SearchView

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))

        searchAutoComplete = searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text) as SearchView.SearchAutoComplete

        setupSearchBarColors(searchAutoComplete)
        setupAutocompleteAdapter(searchAutoComplete)

        // Listen to search view item on click event.
        searchAutoComplete.onItemClickListener = this
    }

    private fun setupAutocompleteAdapter(searchAutoComplete: SearchView.SearchAutoComplete) {
        val newsAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, contactList)
        searchAutoComplete.setAdapter(newsAdapter)
    }

    override fun onItemClick(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val selectedContact = adapterView?.getItemAtPosition(position) as Contact
        openUserDetailsActivity(selectedContact.isFavorite, selectedContact)
        searchMenuItem.collapseActionView()
    }

    private fun setupSearchBarColors(searchAutoComplete: SearchView.SearchAutoComplete) {
        searchAutoComplete.setBackgroundColor(Color.WHITE)
        searchAutoComplete.setTextColor(Color.BLACK)
        searchAutoComplete.setDropDownBackgroundResource(android.R.color.white)
    }

    override fun onResume() {
        super.onResume()
        setupFavoritesContactsRecyclerView()
    }

    override fun onItemClick(contact: Contact, isFavorites: Boolean) {
        openUserDetailsActivity(isFavorites, contact)
    }

    private fun openUserDetailsActivity(isFavorites: Boolean, contact: Contact) {
        val intent = Intent(this, UserDetailsActivity::class.java)
        if (isFavorites) {
            contact.isFavorite = true
        }else{
            /*Since we are supporting 2 Lists (Contact List and Favorite List) we need to check
            if the selected contact was a favorite in the Favorite List.*/
            val contactFavList = prefManager.getFavorites()
            if(contactFavList.isNotEmpty()){
                for(favContact in contactFavList){
                    if(contact.toString() == favContact.toString()){
                        contact.isFavorite = true
                        break
                    }
                }
            }
        }

        intent.putExtra("selected_contact", contact)
        startActivity(intent)
    }
}


